import csv

from Extractor import Extractor


class TestExtractor:
    def test_shouldExtractReturnCSV(self):
        csvs = Extractor().extract(delimiter=";", quote="\"",
                                   pageName="Comparison_of_digital_SLRs")
        reader = csv.reader(csvs[0].split(
            "\n"), delimiter=';', quotechar="\"")
        lineCount = 0
        for record in reader:
            if len(record) > 0:
                lineCount += 1
                assert len(record) == 22
        assert lineCount == 72

    def test_shouldExtractFailWithCommaAndFormat(self):
        csvs = Extractor().extract(delimiter=",", quote="\"",
                                   pageName="Comparison_of_digital_SLRs")
        reader = csv.reader(csvs[0].split(
            "\n"), delimiter=';', quotechar="\"")
        lineCount = 0
        for record in reader:
            if len(record) > 0:
                lineCount += 1
                assert not(len(record) == 22)
        assert lineCount == 72

    def test_shouldExtractFailWithOtherPage(self):
        csvs = Extractor().extract(delimiter=";", quote="\"",
                                   pageName="Comparison_between_Esperanto_and_Ido")
        reader = csv.reader(csvs[0].split(
            "\n"), delimiter=';', quotechar="\"")
        lineCount = 0
        for record in reader:
            if len(record) > 0:
                lineCount += 1
                assert not(len(record) == 22)
        assert not(lineCount == 72)
