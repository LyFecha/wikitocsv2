from Extractor import Extractor


class Main:

    def main():
        extractor = Extractor()
        csvs = extractor.extract(";", "\"", "Comparison_of_digital_SLRs")
        for i in range(len(csvs)):
            csv = csvs[i]
            file = open("Comparison_of_digital_SLRs.csv",
                        "w", encoding="utf-8")
            file.write(csv)
            file.close()
        print(csvs)


Main.main()
