from bs4 import BeautifulSoup
import requests
from bs4.element import NavigableString


class Extractor:

    def extract(self, delimiter=";", quote="\"", pageName=""):
        csvs = []
        doc = requests.get("https://en.wikipedia.org/wiki/" + pageName)
        soup = BeautifulSoup(doc.text, 'html.parser')
        print(soup.title)
        tables = soup.find(id="mw-content-text").findAll(class_="wikitable")
        for table in tables:
            parentNavbox = [parent for parent in table.parents if parent.has_attr(
                "class") and parent["class"].count("navbox") > 0]
            if table.find(class_="metadata") or len(parentNavbox) > 0:
                continue
            csv = ""
            header = ""
            hasTableHeader = False
            for line in table.tbody.findAll("tr"):
                csvLine = ""
                isHeader = False
                for col in line.children:
                    if isinstance(col, NavigableString):
                        continue
                    if col.name == "th" and not(hasTableHeader):
                        isHeader = True
                        header += quote
                        header += col.get_text().replace("\n", "")
                        header += quote + delimiter
                    elif not(col.name == "th"):
                        csvLine += quote
                        csvLine += col.get_text().replace("\n", "")
                        csvLine += quote + delimiter

                hasTableHeader = hasTableHeader or isHeader
                if len(csvLine) > 0:
                    csvLine = csvLine[:-1]
                csv += csvLine
                csv += "\n"
            if len(header) > 0:
                header = header[:-1]
            header += csv
            csvs.append(header)
        return csvs
